# httpcam

HTTP Live Streaming from OS X

Based almost entirely on the sample code from the [Readme](https://github.com/shogo4405/HaishinKit.swift#http-usage) of [HaishinKit.swift](https://github.com/shogo4405/HaishinKit.swift).

http://localhost:8080/camera/playlist.m3u8

![Alt text](./logo.png?raw=true "Logo")

