//
//  ViewController.swift
//  httpcam
//
//  Created by Eric Betts on 3/21/19.
//  Copyright © 2019 Eric Betts. All rights reserved.
//

import Cocoa
import HaishinKit
import AVFoundation

class ViewController: NSViewController {
    var httpService = HLSService(
        domain: "", type: HTTPService.type, name: "HaishinKit", port: 8090
    )
    let httpStream = HTTPStream()
    
    override func loadView() {
        view = NSView()
        view.wantsLayer = true
        view.frame = NSMakeRect(0, 0, 1280, 720)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        httpStream.attachCamera(AVCaptureDevice.default(for: AVMediaType.video))
        httpStream.attachAudio(AVCaptureDevice.default(for: AVMediaType.audio))
        httpStream.publish("camera") //Becomes first segment of url: http://localhost:8090/camera/playlist.m3u8
        
        httpService.startRunning()
        httpService.addHTTPStream(httpStream)
    }
}

