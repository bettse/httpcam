//
//  AppDelegate.swift
//  httpcam
//
//  Created by Eric Betts on 3/21/19.
//  Copyright © 2019 Eric Betts. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    func applicationDidFinishLaunching(aNotification: NSNotification) {
        NSApp.setActivationPolicy(NSApplication.ActivationPolicy.accessory)
        NSApplication.shared.windows.last!.close()
    }
}

